package Server;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.LinkedList;
import java.util.List;

public class ClientHandler extends Thread {
    private Socket socket;
    private List <Socket> clientList = new LinkedList<>();
    private DataInputStream inputStream;
    private DataOutputStream outputStream;
    public ClientHandler (Socket socket,List<Socket> clientList){
        this.socket= socket;
        this.clientList = clientList;
        try {
            inputStream = new DataInputStream(socket.getInputStream());
            outputStream = new DataOutputStream(socket.getOutputStream());
        }catch (IOException e){
            e.printStackTrace();
        }
    }
    @Override
    public void run(){
        while (true){
            try {
                distributeMesssage(inputStream.readUTF());
            }catch (IOException e){
                e.printStackTrace();
            }
        }
    }
    public void distributeMesssage(String msg){
        for (int i=0;i<clientList.size();i++){
            try{
                outputStream = new DataOutputStream(clientList.get(i).getOutputStream());
                this.outputStream.writeUTF(msg);
            }catch (IOException e){
                e.printStackTrace();
            }
        }
    }
}
