package de.hrw.dsalab.distsys.chat;
import javax.swing.*;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
public class TcpListener implements NetworkListener {
    private JTextArea textArea;
    private String nick;
    private Socket socket;
    private DataOutputStream dataOutputStream;
    private DataInputStream dataInputStream;
    public TcpListener(JTextArea textArea, String nick, Socket socket) {
        this.textArea = textArea;
        this.nick = nick;
        this.socket = socket;
        try {
            dataInputStream = new DataInputStream(socket.getInputStream());
            dataOutputStream = new DataOutputStream(socket.getOutputStream());
        }catch (IOException e){
            e.printStackTrace();
        }
    }
    public void sendMessage(String msg){
        try {
            dataOutputStream.writeUTF("<" + nick + "> " + msg);
        }catch (IOException e){
        }
    }
    public void listenForMessage(){
        while (true){
            try {
                messageReceived(dataInputStream.readUTF());
            }catch (IOException e){
                e.printStackTrace();
            }
        }
    }
    @Override
    public void messageReceived ( String msg ) {
        textArea.append(msg + System.getProperty("line.separator"));
    }
}
