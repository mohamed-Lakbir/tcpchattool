package Server;

import de.hrw.dsalab.distsys.chat.Chat;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.LinkedList;
import java.util.List;

public class ChatServer {

    private ServerSocket serverSocket;
    private Socket temp;
    private List <Socket> clientList = new LinkedList<>();
    private int localServerTime;

    public ChatServer(int port) {
        try {
            serverSocket = new ServerSocket(port);

            waitingForClient();

        }catch (IOException e){
            e.printStackTrace();
        }
    }


    public Socket waitingForClient(){
        while (true) {
              try {
                  System.out.println("Waiting for Client");
                  this.temp = serverSocket.accept();
                  clientList.add(temp);

                  new ClientHandler(temp,clientList).start();

              }catch (IOException e){
                  e.printStackTrace();
              }
        }
    }

    public static void main(String[]args){

        new ChatServer(2019);




    }

}
