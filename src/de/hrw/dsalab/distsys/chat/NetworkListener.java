package de.hrw.dsalab.distsys.chat;

public interface NetworkListener {
	
	public void messageReceived(String msg);
	public void sendMessage(String msg);
	public void  listenForMessage();

}
